# First handle infested cases: spawn silverfish next to cracked block in direction anvil is facing.
# Do this first, otherwise the infested block gets replaced!
execute at @s if block ~ ~-1 ~ minecraft:infested_stone if block ~ ~ ~ minecraft:air run summon minecraft:silverfish ^ ^ ^1
execute at @s if block ~ ~-1 ~ minecraft:infested_stone_bricks if block ~ ~ ~ minecraft:air run summon minecraft:silverfish ^ ^ ^1
execute at @s if block ~ ~-1 ~ minecraft:infested_mossy_stone_bricks if block ~ ~ ~ minecraft:air run summon minecraft:silverfish ^ ^ ^1
execute at @s if block ~ ~-1 ~ minecraft:infested_chiseled_stone_bricks if block ~ ~ ~ minecraft:air run summon minecraft:silverfish ^ ^ ^1
execute at @s if block ~ ~-1 ~ minecraft:infested_cracked_stone_bricks if block ~ ~ ~ minecraft:air run summon minecraft:silverfish ^ ^ ^1
execute at @s if block ~ ~-1 ~ minecraft:infested_cobblestone if block ~ ~ ~ minecraft:air run summon minecraft:silverfish ^ ^ ^1

# Blocks that won't be "cracked" but still need to be de-infested
execute at @s if block ~ ~-1 ~ minecraft:infested_cobblestone if block ~ ~ ~ minecraft:air run setblock ~ ~-1 ~ minecraft:cobblestone

# Stone variants
execute at @s if block ~ ~-1 ~ minecraft:stone if block ~ ~ ~ minecraft:air run setblock ~ ~-1 ~ minecraft:cobblestone
execute at @s if block ~ ~-1 ~ minecraft:infested_stone if block ~ ~ ~ minecraft:air run setblock ~ ~-1 ~ minecraft:cobblestone
execute at @s if block ~ ~-1 ~ minecraft:mossy_stone_bricks if block ~ ~ ~ minecraft:air run setblock ~ ~-1 ~ minecraft:mossy_cobblestone
execute at @s if block ~ ~-1 ~ minecraft:infested_mossy_stone_bricks if block ~ ~ ~ minecraft:air run setblock ~ ~-1 ~ minecraft:mossy_cobblestone
execute at @s if block ~ ~-1 ~ minecraft:chiseled_stone_bricks if block ~ ~ ~ minecraft:air run setblock ~ ~-1 ~ minecraft:cobblestone
execute at @s if block ~ ~-1 ~ minecraft:infested_chiseled_stone_bricks if block ~ ~ ~ minecraft:air run setblock ~ ~-1 ~ minecraft:cobblestone
execute at @s if block ~ ~-1 ~ minecraft:cracked_stone_bricks if block ~ ~ ~ minecraft:air run setblock ~ ~-1 ~ minecraft:cobblestone
execute at @s if block ~ ~-1 ~ minecraft:infested_cracked_stone_bricks if block ~ ~ ~ minecraft:air run setblock ~ ~-1 ~ minecraft:cobblestone
# Handle these last in the section, or the cracked stone bricks will immediately convert to cobblestone
execute at @s if block ~ ~-1 ~ minecraft:stone_bricks if block ~ ~ ~ minecraft:air run setblock ~ ~-1 ~ minecraft:cracked_stone_bricks
execute at @s if block ~ ~-1 ~ minecraft:infested_stone_bricks if block ~ ~ ~ minecraft:air run setblock ~ ~-1 ~ minecraft:cracked_stone_bricks

# Blackstone variants
execute at @s if block ~ ~-1 ~ minecraft:polished_blackstone if block ~ ~ ~ minecraft:air run setblock ~ ~-1 ~ minecraft:blackstone
execute at @s if block ~ ~-1 ~ minecraft:cracked_polished_blackstone_bricks if block ~ ~ ~ minecraft:air run setblock ~ ~-1 ~ minecraft:blackstone
execute at @s if block ~ ~-1 ~ minecraft:polished_blackstone_bricks if block ~ ~ ~ minecraft:air run setblock ~ ~-1 ~ minecraft:cracked_polished_blackstone_bricks

# Nether bricks
execute at @s if block ~ ~-1 ~ minecraft:nether_bricks if block ~ ~ ~ minecraft:air run setblock ~ ~-1 ~ minecraft:cracked_nether_bricks

# Set HasCracked tag to stop the anvil acting twice on the same block (this can happen if it is falling slowly)
execute at @s unless block ~ ~-1 ~ minecraft:air run tag @s add HasCracked
