# Stonecracker

A simple datapack to let anvils which fall on stone type blocks crack the stone.

- Stone converts to cobblestone
- Stone bricks convert to cracked stone bricks
- Cracked stone bricks crumble further into cobblestone
- Mossy stone bricks convert to mossy cobblestone
- Chiseled stone bricks convert to cobblestone
- Polished blackstone converts to blackstone
- Polished blackstone bricks convert to cracked polished blackstone bricks
- Cracked polished blackstone bricks crumble further into blackstone
- Nether bricks convert to cracked nether bricks

Deepslate is intentionally immune due to its higher strength.

Watch out, dropping an anvil on an infested block will crack it open and let the silverfish out!
